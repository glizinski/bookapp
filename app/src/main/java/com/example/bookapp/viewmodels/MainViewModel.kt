package com.example.bookapp.viewmodels

import androidx.lifecycle.ViewModel
import com.example.bookapp.interfaces.AuthorDescriptionCallback
import com.example.bookapp.models.AuthorDescriptionResponseItem
import com.example.bookapp.models.AuthorsResponseItem
import com.example.bookapp.rest.RestSingleton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel : ViewModel() {

    fun callAuthors(callback : AuthorDescriptionCallback) {

        val call : Call<ArrayList<AuthorsResponseItem>?>? = RestSingleton.restServiceBookApp?.getAuthors()

        call?.enqueue(object : Callback<ArrayList<AuthorsResponseItem>?> {
            override fun onResponse(
                call: Call<ArrayList<AuthorsResponseItem>?>,
                response: Response<ArrayList<AuthorsResponseItem>?>
            ) {
                callback.getResponseListAuthorsResponseItem(response.body() ?: arrayListOf())
            }

            override fun onFailure(call: Call<ArrayList<AuthorsResponseItem>?>, t: Throwable) {
                callback.getResponseListAuthorsResponseItem(null)
            }
        })
    }

    fun callDescription(authorSlug : String, callback : AuthorDescriptionCallback) {
        val callDescription : Call<AuthorDescriptionResponseItem?>? = RestSingleton.restServiceBookApp?.getDescription(authorSlug)

        callDescription?.enqueue(object : Callback<AuthorDescriptionResponseItem?> {
            override fun onResponse(
                call: Call<AuthorDescriptionResponseItem?>?,
                response: Response<AuthorDescriptionResponseItem?>?
            ) {
                callback.getResponseListAuthorDescriptionResponseItem(response?.body())
            }

            override fun onFailure(call: Call<AuthorDescriptionResponseItem?>, t: Throwable) {
                callback.getResponseListAuthorDescriptionResponseItem(null)
            }
        })
    }

}