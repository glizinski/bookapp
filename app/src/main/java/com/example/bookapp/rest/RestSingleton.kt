package com.example.bookapp.rest

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RestSingleton {
    var restServiceBookApp : RestServiceBookApp? = null

    init {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val client : OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .build()
        val retrofitBookApp = Retrofit.Builder()
            .client(client)
            .baseUrl("https://wolnelektury.pl")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        restServiceBookApp = retrofitBookApp.create(RestServiceBookApp::class.java)
    }
}