package com.example.bookapp.rest

import com.example.bookapp.models.AuthorDescriptionResponseItem
import com.example.bookapp.models.AuthorsResponseItem
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface RestServiceBookApp {
    @GET("/api/authors/?format=json")
    fun getAuthors() : Call<ArrayList<AuthorsResponseItem>?>

    @GET("api/authors/{authorName}")
    fun getDescription(
        @Path("authorName")
        authorName : String) : Call<AuthorDescriptionResponseItem?>?
}