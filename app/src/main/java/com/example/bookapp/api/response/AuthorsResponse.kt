package com.example.bookapp.api.response

import com.example.bookapp.models.AuthorsResponseItem

class AuthorsResponse : ArrayList<AuthorsResponseItem>()