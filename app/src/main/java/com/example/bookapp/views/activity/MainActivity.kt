package com.example.bookapp.views.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bookapp.R
import com.example.bookapp.databinding.MainBinding
import com.example.bookapp.interfaces.AuthorDescriptionCallback
import com.example.bookapp.models.AuthorDescriptionResponseItem
import com.example.bookapp.models.AuthorsResponseItem
import com.example.bookapp.viewmodels.MainViewModel
import com.example.bookapp.views.adapter.AuthorAdapter
import android.text.Editable

import android.text.TextWatcher

import android.util.Log


class MainActivity : AppCompatActivity(), AuthorDescriptionCallback {

    private var busyClick : Boolean = false
    private lateinit var adapter : AuthorAdapter
    private lateinit var viewModel : MainViewModel

    private lateinit var binding : MainBinding

    var allAuthorsList : ArrayList<AuthorsResponseItem>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        initViewModel()

        binding.progressBar.visibility = View.VISIBLE

        viewModel.callAuthors(this)

        searchByAuthor()
    }

    private fun searchByAuthor() {

        binding.editText.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                Log.e("TextWatcherTest", "onTextChanged:\t$s")
                filterList()
            }
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }
            override fun afterTextChanged(s: Editable) {

            }
        })
    }

    private fun filterList() {
        val textFromEditText = binding.editText.text.toString()

        allAuthorsList?.let { it ->
            val copyOfAuthorsList = ArrayList(it)
            val filteredListByNameOfAuthor = copyOfAuthorsList.filter {
                it.name?.contains(textFromEditText, true) ?: false
            }
            setAuthorAdapter(ArrayList(filteredListByNameOfAuthor))
        }
    }

    override fun onStart() {
        super.onStart()
        busyClick = false
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
    }

    private fun setAuthorAdapter(result : ArrayList<AuthorsResponseItem>) {
        adapter = AuthorAdapter(result, this)
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = adapter
        adapter?.notifyDataSetChanged()

        binding.progressBar.visibility = View.GONE
    }

    override fun getAuthorDescription(author : AuthorsResponseItem) {

        binding.progressBar.visibility = View.VISIBLE

        viewModel.callDescription(author.slug ?: "", this)
    }

    override fun getResponseListAuthorsResponseItem(list : ArrayList<AuthorsResponseItem>?) {
        allAuthorsList = list
        setAuthorAdapter(list ?: arrayListOf())
        binding.editText.visibility = View.VISIBLE
    }

    override fun getResponseListAuthorDescriptionResponseItem(item : AuthorDescriptionResponseItem?) {

        binding.progressBar.visibility = View.GONE

        if (!busyClick) {
            busyClick = true
            goToAuthorDescriptionActivity(item)
        }
    }

    private fun goToAuthorDescriptionActivity(item : AuthorDescriptionResponseItem?) {
        val intent = Intent(this, AuthorDescriptionActivity::class.java)
        val bundle = Bundle()
        bundle.putSerializable(EXTRA_AUTHOR_NAME, item)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    companion object {
        const val EXTRA_AUTHOR_NAME = "EXTRA_AUTHOR_NAME"
    }
}
