package com.example.bookapp.views.activity

import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import androidx.databinding.DataBindingUtil.*
import com.bumptech.glide.Glide
import com.example.bookapp.R
import com.example.bookapp.databinding.AuthorDescriptionBinding
import com.example.bookapp.models.AuthorDescriptionResponseItem

class AuthorDescriptionActivity : AppCompatActivity() {

    private lateinit var binding : AuthorDescriptionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = setContentView(this, R.layout.activity_author_description)

        val intent = intent
        val item = intent.getSerializableExtra(MainActivity.EXTRA_AUTHOR_NAME) as AuthorDescriptionResponseItem?

        if (item != null) setAuthorDescription(item)
    }

    private fun setAuthorDescription(author: AuthorDescriptionResponseItem) {

        val authorName = binding.authorName
        val description = binding.description

        authorName.text = author.name

        author.description?.let {
            description.text = HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY)
        }

        setGlideImage()

    }

    private fun setGlideImage(){

        val descriptionImage = binding.descriptionImage

        Glide
            .with(this)
            .load(getElementOfList())
            .fitCenter()
            .into(descriptionImage)
    }

    private fun getElementOfList() : String {

        val listOfImages : ArrayList<String> = ArrayList()
        listOfImages.add("https://cdn-icons-png.flaticon.com/512/864/864702.png")
        listOfImages.add("https://cdn-icons-png.flaticon.com/512/327/327116.png")
        listOfImages.add("https://cdn-icons-png.flaticon.com/512/2232/2232688.png")
        listOfImages.add("https://cdn-icons-png.flaticon.com/512/3557/3557574.png")

        return listOfImages.random()
    }
}