package com.example.bookapp.views.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.bookapp.R
import com.example.bookapp.databinding.AuthorRow
import com.example.bookapp.interfaces.AuthorDescriptionCallback
import com.example.bookapp.models.AuthorsResponseItem
import com.mikhaellopez.rxanimation.RxAnimation
import com.mikhaellopez.rxanimation.press

class AuthorAdapter(private var arrayList: ArrayList<AuthorsResponseItem>, private var callback : AuthorDescriptionCallback) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val adapterRow = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context),
            R.layout.author_row,
            parent,
            false
        )
        return AuthorView(adapterRow as AuthorRow, callback)
    }

    class AuthorView(private var binding : AuthorRow, private var callback : AuthorDescriptionCallback) : RecyclerView.ViewHolder(binding.root) {
        fun bind(variable : AuthorsResponseItem){
            val cardView = binding.cardView
            val authorName = binding.authorName
            val authorUrl = binding.authorUrl

            val imageView = binding.image
            imageView.setImageResource(getElementOfList())

            authorName.text = variable.name
            authorUrl.text = variable.url

            cardView.setOnClickListener{
                RxAnimation.from(it).press(duration = 300).subscribe {
                    callback.getAuthorDescription(variable)
                }
            }
        }

        private fun getElementOfList() : Int {

            val listOfImages : ArrayList<Int> = ArrayList()
            listOfImages.add(R.drawable.author_image1)
            listOfImages.add(R.drawable.author_image2)
            listOfImages.add(R.drawable.author_image3)
            listOfImages.add(R.drawable.author_image4)
            listOfImages.add(R.drawable.author_image5)

            return listOfImages.random()
        }

    }

    override fun getItemCount(): Int {
        return if (arrayList.isNullOrEmpty()) 0 else arrayList.size
    }

    override fun onBindViewHolder(viewHolder : RecyclerView.ViewHolder, position: Int) {
        val holder = viewHolder as AuthorView
        val result = arrayList[position]
        holder.bind(result)
    }
}