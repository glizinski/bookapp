package com.example.bookapp.interfaces

import com.example.bookapp.models.AuthorDescriptionResponseItem
import com.example.bookapp.models.AuthorsResponseItem

interface AuthorDescriptionCallback {
    fun getAuthorDescription(author : AuthorsResponseItem)
    fun getResponseListAuthorsResponseItem(list : ArrayList<AuthorsResponseItem>?)
    fun getResponseListAuthorDescriptionResponseItem(item : AuthorDescriptionResponseItem?)
}