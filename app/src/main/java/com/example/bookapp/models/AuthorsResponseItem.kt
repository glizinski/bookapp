package com.example.bookapp.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class AuthorsResponseItem(
    @SerializedName("href")
    var href : String?,
    @SerializedName("name")
    var name : String?,
    @SerializedName("slug")
    var slug : String?,
    @SerializedName("url")
    var url : String?,
    ) : Serializable