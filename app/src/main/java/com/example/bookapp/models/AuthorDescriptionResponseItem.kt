package com.example.bookapp.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class AuthorDescriptionResponseItem(
    @SerializedName("description")
    var description: String?,
    @SerializedName("name")
    var name: String?,
    @SerializedName("sort_key")
    var sortKey: String?,
    @SerializedName("url")
    var url: String?
) : Serializable